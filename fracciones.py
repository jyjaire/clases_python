import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Circle

fig = plt.figure()

ax = fig.add_axes([0,0,1,1])

print("Ingrese el Numerador de la fracción")
numerador = input()

print("Ingrese el Denominador de la fracción")
denominador = input()

rec = []
x = 0
#Franccion con numerador menor que denominador
if int(numerador) < int(denominador):
	for i in range(0,int(denominador)):

		if i >= 0 and i < int(numerador):

			rec.append(Rectangle((0+x, 20), 3, 10, fc='red', alpha=.5, linewidth=2, edgecolor='black'))

		else:

			rec.append(Rectangle((0+x, 20), 3, 10, fc='white', alpha=.5, linewidth=2, edgecolor='black'))

		x += 3

#fraccion con numerador mayor que el denominador
if int(numerador) > int(denominador):
	nu = 0
	
	denominadorN = int(denominador) * 2
	

	if int(numerador) < denominadorN:

		div = denominadorN / 2
		x = 0
		for n in range(0,int(div)):

			rec.append(Rectangle((0+x, 20), 3, 10, fc='red', alpha=.5, linewidth=2, edgecolor='black'))
			
			nu += 1 

			x += 3
		
		for ni in range(int(div),denominadorN):
			if nu < int(numerador):
				rec.append(Rectangle((3+x, 20), 3, 10, fc='red', alpha=.5, linewidth=2, edgecolor='black'))
				nu += 1 
			else:

				rec.append(Rectangle((3+x, 20), 3, 10, fc='white', alpha=.5, linewidth=2, edgecolor='black'))
			
		
			x += 3
		


for r in rec:
	ax.add_patch(r)

txt = numerador
ax.text(1.5,17, txt, horizontalalignment='left', verticalalignment='top', fontsize=20, color='blue')
txt = "__"
ax.text(1,17, txt, horizontalalignment='left', verticalalignment='top', fontsize=20, color='red')
txt = denominador
ax.text(1.5,14.5, txt, horizontalalignment='left', verticalalignment='top', fontsize=20, color='blue')

ax.set_aspect('equal', adjustable='box')

plt.axis([0, 38, 0, 38])
plt.axis('off')
mng = plt.get_current_fig_manager()

plt.show()