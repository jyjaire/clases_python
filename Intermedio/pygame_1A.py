import pygame
from pygame.locals import *


#Constantes
SCREEN_WIDTH = 640
SCREN_HEIGHT = 480

def main():
	pygame.init()
	screen = pygame.display.set_mode((SCREEN_WIDTH, SCREN_HEIGHT))
	pygame.display.set_caption("Tutorial Pygame 1a")

	fondo = pygame.image.load("matrix.png").convert()
	imagen = pygame.image.load("sable.png").convert_alpha()

	screen.blit(fondo,(0,0))
	screen.blit(imagen,(10,10))
	x = 10
	y = 10
	pygame.display.flip()

	reloj = pygame.time.Clock()
	while True:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				sys.exit()

		pulsada = pygame.key.get_pressed()

		if pulsada[K_w]:
			y -= 5
		if pulsada[K_s]:
			y += 5
		if pulsada[K_a]:
			x -= 5
		if pulsada[K_d]:
			x += 5

		reloj.tick(25)

		screen.blit(fondo,(0,0))
		screen.blit(imagen, (x,y))

		pygame.display.update()

if __name__ == "__main__":
	main()