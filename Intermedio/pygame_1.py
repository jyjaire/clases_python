#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ---------------------------
# Importacion de los módulos
# ---------------------------

import pygame
from pygame.locals import *

# -----------
# Constantes
# -----------

SCREEN_WIDTH = 640
SCREEN_HEIGHT = 480

# ------------------------------
# Clases y Funciones utilizadas
# ------------------------------


# ------------------------------
# Funcion principal del juego
# ------------------------------


def main():
    pygame.init()
    # creamos la ventana y le indicamos un titulo:
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    pygame.display.set_caption("tutorial pygame")
    fondo = pygame.image.load("matrix.png").convert()
    imagen = pygame.image.load("sable.png").convert_alpha()
    # Indicamos la posicion de las "Surface" sobre la ventana
    screen.blit(fondo, (0, 0))
    screen.blit(fondo, (10, 10))

    x = 10
    y = 10
    # se muestran lo cambios en pantalla
    pygame.display.flip()
    # el bucle principal del juego
    reloj = pygame.time.Clock()
    while True:
        # Posibles entradas del teclado y mouse
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        pulsada = pygame.key.get_pressed()

        if pulsada[K_w]:
	        y -= 5
        if pulsada[K_s]:
	        y += 5
        if pulsada[K_a]:
	        x -= 5
        if pulsada[K_d]:
            x += 5
        reloj.tick(25)
        screen.blit(fondo, (0, 0))
        screen.blit(imagen,(x,y))

        pygame.display.update()

if __name__ == "__main__":
    main()